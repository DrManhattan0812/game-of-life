// $ cd Documents/Cours/LPRGI/Algo/JeuDeLaVie

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <math.h>

// REFAIRE LE DIAGRAMME

// Ajouter un compteur de vies et de morts ?
// Contrôler la conformité à l'analyse.

// corriger DAY&NIGHT CONTROLER CELLULES MORTES. FAIT

// Ajouter la possibilité de choisir la taille // FAIT --> Ajouter à l'analyse. FAIT
// Ajouter la possibilité de choisir le nombre de cycles. // FAIT -->  Ajouter à l'analyse FAIT
// Ajouter la possibilité de générer un pourcentage de cellules vivantes ou mortes sur la grille. FAIT
// Corriger le contrôle des cellules mortes de D&N = FAIT --> modif sur anal FAIT

void initFill(char** gameMat, int** neighborMat, int gamesize, int PourCell)
{
    int nbCellcpt = 0;
    double nbCellAlive = (gamesize * gamesize) * PourCell / 100;

    // Initialisation
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {   

            if (nbCellcpt < nbCellAlive)
            {
               gameMat[i][j] = 'o';
            }
            else
            {
               gameMat[i][j] = ' ';
            }
           
            neighborMat[i][j] = 0;
            nbCellcpt++;
        }
    }
}

void initMelange(char** gameMat, int** neighborMat, int gamesize)
{
    int nbaleatoire, nbaleatoire2;
    char temp;
    srand(time(NULL)); 

    //Mélange
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {

            nbaleatoire = (rand() % ((gamesize-1) - 0 + 1)) + 0;
            nbaleatoire2 = (rand() % ((gamesize-1) - 0 + 1)) + 0;

            temp = gameMat[i][j];
            gameMat[i][j] = gameMat[nbaleatoire][nbaleatoire2];
            gameMat[nbaleatoire][nbaleatoire2] = temp;

        }
    }
}

void init_game (char** gameMat, int** neighborMat, int gamesize, int PourCell)
{

    initFill(gameMat, neighborMat, gamesize, PourCell);
    initMelange(gameMat, neighborMat, gamesize);

    /*
    
    gameMat[1][3] = 'o';
    gameMat[2][3] = 'o';
    gameMat[3][4] = 'o';
    gameMat[1][4] = 'o';
    gameMat[2][5] = 'o';
    gameMat[3][5] = 'o';
     
    */
}

void checkNeighborUp (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i-1][j] == 'o')
    {
        
        neighborMat[i][j]++;
    }
}

void checkNeighborLeft (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i][j-1] == 'o')
    {
        neighborMat[i][j]++;
    }
}

void checkNeighborRight (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i][j+1] == 'o')
    {
        neighborMat[i][j]++;
    }
}

void checkNeighborDown (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i+1][j] == 'o')
    {
        neighborMat[i][j]++;
    }
}

void checkNeighborLeftDown (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i+1][j-1] == 'o')
    {
        neighborMat[i][j]++;
    }
}

void checkNeighborLeftUp (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i-1][j-1] == 'o')
    {
        neighborMat[i][j]++;
    }
}

void checkNeighborRightUp (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i-1][j+1] == 'o')
    {
        neighborMat[i][j]++;
    }
}

void checkNeighborRightDown (int** neighborMat, char** gameMat, int i, int j)
{
    if (gameMat[i+1][j+1] == 'o')
    {
        neighborMat[i][j]++;
    }
}

// Vérifies le nombre de voisins par cellules.
void neighborRefresh(int** neighborMat, char** gameMat, int gamesize)
{
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {      
            // Conditions pour éviter de contrôler des cellules positionnés en dehors du jeu.
            if (i > 0)
            {
                checkNeighborUp(neighborMat, gameMat,i, j);
                
                if (j > 0)
                {
                    checkNeighborLeftUp(neighborMat, gameMat, i, j);
                }
                
                if (j < gamesize - 1)
                {
                    checkNeighborRightUp(neighborMat, gameMat, i, j);
                }
            }
            
            if (i < gamesize - 1)
            {
                checkNeighborDown(neighborMat, gameMat, i, j);
                
                if (j > 0)
                {
                    checkNeighborLeftDown(neighborMat, gameMat, i, j);
                }
                
                if (j < gamesize - 1)
                {
                    checkNeighborRightDown(neighborMat, gameMat, i, j);
                }
            }
            
            if (j > 0)
            {
                checkNeighborLeft(neighborMat, gameMat, i, j);
            }
            
            if (j < gamesize - 1)
            {
                checkNeighborRight(neighborMat, gameMat, i, j);
            }
        }
    }
}

void updateGameOfLife(char** gameMat, int** neighborMat, int gamesize)
{
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {
            // A regrouper sous forme de sous-fonctions ?
            // Celulles vivante
            if (gameMat[i][j] == 'o')
            {
                // La cellule survit
                if ((neighborMat[i][j] == 3) || (neighborMat[i][j] == 2))
                {
                    gameMat[i][j] = 'o'; // en gros on l'affiche ou reste affiché.
                }
                
                // La cellule meurt d'étouffement
                else if(neighborMat[i][j] > 3) // Si supérieur à 4 elle meurt dans tous les cas.
                {
                    gameMat[i][j] = ' '; // On met un espace pour faire croire que c'est vide.
                }
                
                // La cellule meurt d'isolement
                else if (neighborMat[i][j] < 2) // en gros 1 ou 0 elle meurt
                {
                    gameMat[i][j] = ' '; // On met un espace pour faire croire que c'est vide.
                }
            }
            
            //Cellulles morte
            else if (gameMat[i][j] == ' ')
            {
                if (neighborMat[i][j] == 3)
                {
                    gameMat[i][j] = 'o';
                }
            } 
        }
    }
}

void updateDayAndNight(char** gameMat, int** neighborMat, int gamesize)
{
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {
            // A regrouper sous forme de sous-fonctions ?
            // Celulles vivante
            if (gameMat[i][j] == 'o')
            {
                // La cellule survit
                if ((neighborMat[i][j] > 2) && (neighborMat[i][j] < 9))
                {
                    gameMat[i][j] = 'o'; // en gros on l'affiche ou reste affiché.

                    if ((neighborMat[i][j] == 5) || (neighborMat[i][j] == 4))
                    {
                        gameMat[i][j] = ' ';
                    }
                }
                else
                {
                    gameMat[i][j] = ' ';
                }
            }
            
            //Cellulles morte
            else if (gameMat[i][j] == ' ')
            {
                if ((neighborMat[i][j] <= 2) || (neighborMat[i][j] == 5))
                {
                    gameMat[i][j] = 'o';
                
                }
                else
                {
                    gameMat[i][j] = ' ';
                }
            }
        }
    }
}

// Mise à jour du contenu de la matrice en fonction de l'analyse des voisins.
void updateGame(char** gameMat, int** neighborMat, int choice, int gamesize)
{
    if(choice == 1)
    {
        updateGameOfLife(gameMat, neighborMat, gamesize);
    }
    else if (choice == 2)
    {
        updateDayAndNight(gameMat, neighborMat, gamesize);
    }
}

void print_game(char** gameMat, int gamesize)
{
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {
            printf("%c", gameMat[i][j]);
        }
        printf("\n");
    }
}

void reset_neighbors(int** neighborMat, int gamesize)
{
    for (int i = 0; i < gamesize; i++)
    {
        for (int j = 0; j < gamesize; j++)
        {
            neighborMat[i][j] = 0;
        }
    }
}

int MenuChoice(int choice){

    while ((choice != 1) && (choice != 2))
    {
        printf("Bienvenu sur jeuDeLaVie. Jouez au jeu original, ou testez Day & Night : \n\n - 1 : Jeu de la vie \n - 2 : Day&Night\nVotre choix : ");

        scanf("%d", &choice);
        system("cls");
    }

    return choice;
}

int MenuNbCell(int gamesize){

    while (gamesize < 10)
    {
        printf("Combien de cellules souhaitez vous par ligne ? Le nombre exige est de minimum 10.\nVotre choix : ");
        scanf("%d", &gamesize);
        system("cls");
    }

    return gamesize;
}

int MenuPourCell(int PourCell){

    while (PourCell == 0)
    {
        printf("Quel pourcentage des cellules doivent etre vivantes ?\nVotre choix : ");
        scanf("%d", &PourCell);
        system("cls");
    }

    return PourCell;
}

int MenuCycles(int nbCycles){

    while (nbCycles == 0)
    {
        printf("Combien de cycles souhaitez vous effectuer ?\nVotre choix : ");
        scanf("%d", &nbCycles);
        system("cls");
    }

    return nbCycles;
}

int display_menu(int *gamesize, int *nbCycles, int *PourCell)
{
    int choice = 0;
    *gamesize = 0;
    *nbCycles = 0;
    *PourCell = 0;

    choice = MenuChoice(choice);
    *gamesize = MenuNbCell(*gamesize);
    *PourCell = MenuPourCell(*PourCell);
    *nbCycles = MenuCycles(*nbCycles);

    return choice;
}

void gameLoop ()
{
    int choice, gamesize, nbCycles, cpt, PourCell;
    choice = display_menu(&gamesize, &nbCycles, &PourCell);
    char **gameMat = malloc (sizeof(char*) * gamesize);
    int **neighborMat = malloc (sizeof(int*) * gamesize);
    
    for (int i = 0; i < gamesize; i++)
    {
        gameMat[i] = (char*) malloc(sizeof(char)*gamesize);
        neighborMat[i] = (int*) malloc(sizeof(int)*gamesize);
    }
    
    init_game(gameMat, neighborMat, gamesize, PourCell);

    cpt = 0;

    while (cpt < nbCycles)
    {
        neighborRefresh(neighborMat, gameMat, gamesize);
        updateGame(gameMat, neighborMat, choice, gamesize);
        print_game(gameMat, gamesize);
        reset_neighbors(neighborMat, gamesize);

        Sleep(200);
        system("cls");

        cpt++;
    }

    for(int i = 0; i < gamesize; ++i)
    {
        free(gameMat[i]);
    }

    free(gameMat);

        for(int i = 0; i < gamesize; ++i)
    {
        free(neighborMat[i]);
    }

    free(neighborMat);
}

int main(int argc, const char * argv[]) {
    
    // Deux tableaux (matrices), l'une qui affiche et l'autre qui permet de connaître, par case, le nombre de voisins.
    

    
    gameLoop();
    // Créer une condition d'arrêt sur une touche
    // Créer une fonction game loop
   
    
    // Regrouper dans une fonction endgame.


    
    return 0;
}
